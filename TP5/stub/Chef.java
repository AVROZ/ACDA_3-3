import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.Objects;

/** feuille du motif composite */
public class Chef extends Person {
    ArrayList<Person> subList = new ArrayList<>();
    int sum;

    public boolean addSubalterne(Person p){
        return subList.add(p);
    }


    
    /** constructeur
     *
     * @param n fun factor
     *
     */ 
    public Chef(int n){
        super(n);
        // d'autres choses peut-être.
    }

    
    /** 
     * La meilleure fête avec moi, c'est la meilleure fête sans mes subalternes pour eux plus moi. 
     *
     * @return retourne la valeur de la meilleure fête en invitant seulement les gens dont cette personne est le ou la supérieure hiérarchique, mais pas elle.
     *
     */ 
    public int bestPartyWithoutMe(){
        int sum = 0;

        for (int i = 0; i < subList.size() ; i++) {
            sum += subList.get(i).getFunFactor();
        }

        return sum;
    }

    /**
     *  La meilleure fête est soit sans moi (c'est l'union des meilleures fêtes de mes subalternes). 
     *  soit c'est la meilleure fête avec moi.
     *
     * @return la valeur de la meilleure fête en invitant seulement les gens dont cette personne est le ou la supérieure hiérarchique (peut-être avec elle).
     *
     */ 
    public int bestParty(){
        int sum = 0;

        sum += this.getFunFactor();

        return sum;
    }
    
}


