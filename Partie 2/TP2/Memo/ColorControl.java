import java.awt.Color;
import java.awt.event.*;

public class ColorControl implements MouseWheelListener {

    private Fenetre fenetre;
    private int color = 0;      // 0 is for white and 1 is for red

    @Override
    public void mouseWheelMoved(MouseWheelEvent e) {
        e.consume();
        fenetre = (Fenetre) e.getSource();
        // if (color == 0) {
        //     fenetre.zone.setBackground(Color.RED);
        //     fenetre.repaint();
        //     color++;
        //     System.out.println("+");
        // } else if (color == 1) {
        //     fenetre.zone.setBackground(Color.WHITE);
        //     fenetre.repaint();
        //     color--;
        //     System.out.println("-");
        // }
        // System.out.println(color);
        switch (color) {
            case 0:
                fenetre.zone.setBackground(Color.RED);
                fenetre.repaint();
                color++;
                break;

            case 1:
                fenetre.zone.setBackground(Color.WHITE);
                fenetre.repaint();
                color--;
                break;
        
            default:
                break;
        }
    }
    
    public int getColor() {
        return color;
    }
}