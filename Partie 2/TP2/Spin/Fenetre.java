/**
 * Fenetre class definition
 * generated from StarUML project Spin on 17/06/2012
 *
 * @author Luc Hernandez
 */




public class Fenetre extends javax.swing.JFrame {
  public Fenetre() {
    add(new Dessin());
    addWindowListener(new ControleFenetre());
    setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
    setSize(200, 200);
    setLocation(200, 200);
  }
}
