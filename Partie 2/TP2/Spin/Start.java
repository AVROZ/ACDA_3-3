/**
 * Start class definition
 * generated from StarUML project Spin on 17/06/2012
 *
 * @author Luc Hernandez
 */




public class Start implements Runnable {
  public static void main(String[] args) {
  	(new Thread(new Start())).start();
  }
  public void run() {
    (new Fenetre()).setVisible(true);
  }
}
