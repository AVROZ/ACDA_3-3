/**
 * Triangle class definition
 * generated from StarUML project Spin on 17/06/2012
 *
 * @author Luc Hernandez
 */




public class Triangle extends java.awt.Polygon {
  public Triangle() {
    addPoint(0, 50);
    addPoint(20, -20);
    addPoint(0, -10);
    addPoint(-20, -20);
  }

  public void spin() {
    int tmp;
    for(int i = 0; i<npoints; i++) {
      tmp = xpoints[i];
      xpoints[i] = -ypoints[i];
      ypoints[i] = tmp;
    }
  }
}
