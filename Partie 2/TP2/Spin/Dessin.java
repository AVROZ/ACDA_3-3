/**
 * Dessin class definition
 * generated from StarUML project Spin on 17/06/2012
 *
 * @author Luc Hernandez
 */




public class Dessin extends javax.swing.JComponent {
  private Triangle modele;
  public Dessin() {
  	setBackground(java.awt.Color.WHITE);
  	setOpaque(true);
    modele = new Triangle();
    addMouseListener(new ControleDessin(modele));
  }

  public void paintComponent(java.awt.Graphics g) {
    java.awt.Graphics g2 = g.create();
    if (isOpaque()) {
    	g2.setColor(getBackground());
  		g2.fillRect(0, 0, getWidth(), getHeight());
  	}
    g2.translate(getWidth()/2, getHeight()/2);
    g2.setColor(java.awt.Color.BLUE);
    if (g2 instanceof java.awt.Graphics2D)
    	((java.awt.Graphics2D) g2).setRenderingHint(java.awt.RenderingHints.KEY_ANTIALIASING,
    	                                             java.awt.RenderingHints.VALUE_ANTIALIAS_ON);
    g2.fillPolygon(modele);
  }
}
