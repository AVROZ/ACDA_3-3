/**
 * ControleDessin class definition
 * generated from StarUML project Spin on 17/06/2012
 *
 * @author Luc Hernandez
 */




public class ControleDessin implements java.awt.event.MouseListener {
  private Triangle modele;

  public ControleDessin(Triangle m) {
    modele = m;
  }

  public void mouseClicked(java.awt.event.MouseEvent e) {
    modele.spin();
    ((Dessin) e.getSource()).repaint();
  }

  public void mouseEntered(java.awt.event.MouseEvent e) {}
  public void mouseExited(java.awt.event.MouseEvent e) {}
  public void mousePressed(java.awt.event.MouseEvent e) {}
  public void mouseReleased(java.awt.event.MouseEvent e) {}
}
