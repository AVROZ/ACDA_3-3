import javax.swing.*;
import java.awt.*;

/**
 * 
 */
public class Fenetre {
    JFrame window;
    Triangle triangle;
    WindowListener winListener;

    /**
     * Default constructor
     */
    public Fenetre() {
        window = new JFrame();
        triangle = new Triangle();
        winListener = new WindowListener(triangle);
        
        window.setSize(500, 500);
        window.setLocation(100, 100);
        window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        window.add(triangle);

        window.setVisible(true);
    }

}