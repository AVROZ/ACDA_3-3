import javax.swing.JComponent;
import javax.swing.*;
import java.awt.*;

/**
 * 
 */
public class Triangle extends JComponent {

    /**
     * 
     */
    private int[] x;

    /**
     * 
     */
    private int[] y;

    /**
     * 
     */
    private int n;

    /**
     * Default constructor
     */
    public Triangle() {
        int[] xTriangle = {0, 150, 300};
        int[] yTriangle = {300, 0, 300};
        
        x = xTriangle;
        y = yTriangle;
        n = 3;
    }

    /**
     * 
     */
    @Override
    protected void paintComponent(Graphics pinceau) {
        Graphics secondPinceau = pinceau.create();

        if (this.isOpaque()) {
            secondPinceau.setColor(this.getBackground());
            secondPinceau.fillRect(0, 0, this.getWidth(), this.getHeight());
        }

        secondPinceau.setColor(Color.BLUE);
        secondPinceau.fillPolygon(x, y, n);
    }

    /**
     * @param int[] 
     * @param int[] 
     * @param int
     */
    public void quarterRotation(int[] x, int[] y, int n) {
        // TODO implement here
    }

}